module omcmap.color;

struct RGBA
{
    ubyte r;
    ubyte g;
    ubyte b;
    ubyte a = ubyte.max;
}

ubyte[Length * 4] colorsToBytes(size_t Length)(RGBA[Length] colors)
{
    return cast(ubyte[Length * 4]) colors;
}
