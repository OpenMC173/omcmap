module omcmap.util;

import std.conv;
import std.file;
import std.path;
import std.stdio;
import std.string : format;

import datefmt : datefmt = format;

import omcmap.test;


bool validateDirectory(string path)
{
    if (!isValidPath(path))
    {
        writeln("Invalid folder name:");
        writeln("\t" ~ path);
        return false;
    }

    if (!exists(path) || !isDir(path))
    {
        writeln("Folder does not exist:");
        writeln("\t" ~ path);
        return false;
    }

    return true;
}

string indent(string s, string indentation)
{
    import std.string : replace, endsWith;
    string temp = s;
    bool hadTrailingNewline = endsWith(temp, "\n");
    bool hasTrailingNewline = hadTrailingNewline;
    while (hasTrailingNewline)
    {
        temp = temp[0 .. temp.length - 1];
        hasTrailingNewline = endsWith(temp, "\n");
    }
    string ret = indentation ~ temp;
    ret = ret.replace("\n", "\n" ~ indentation);
    if (hadTrailingNewline)
    {
        ret ~= "\n";
    }
    return ret;
}
unittest {
    string initial = "a\nb\nc";
    string actual = indent(initial, "    ");
    string expected = "    a\n    b\n    c";
    expect(expected, actual);
}

string insertEveryNChars(string s, char toInsert, uint interval)
{
    string ret = "";
    uint numSoFar = 0;
    foreach (c; s)
    {
        if (numSoFar == interval)
        {
            ret ~= toInsert;
            numSoFar = 0;
        }
        ret ~= c;
        numSoFar++;
    }
    return ret;
}

/// Flips a row-major array of bytes such that the last row appears first, and the first row appears last.
ubyte[Length] flipY(size_t Length)(scope const ubyte[Length] bytes, uint width, uint height)
in { assert(bytes.length == width * height); }
do
{
    ubyte[Length] dest;

    uint srcChunkIdx = (height - 1) * width;
    uint destChunkIdx = 0;
    uint rowNum = 0;
    while (rowNum < height)
    {
        dest[destChunkIdx .. destChunkIdx + width] = bytes[srcChunkIdx .. srcChunkIdx + width];

        srcChunkIdx -= width;
        destChunkIdx += width;
        rowNum++;
    }
    return dest;
}
unittest
{
    ubyte[9] test = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    ubyte[9] expected = [7, 8, 9, 4, 5, 6, 1, 2, 3];
    expect(expected, flipY(test, 3, 3));
}
unittest
{
    ubyte[32] input = [
         1,  2,  3,  4,  5,  6,  7,  8,
         9, 10, 11, 12, 13, 14, 15, 16,
        17, 18, 19, 20, 21, 22, 23, 24,
        25, 26, 27, 28, 29, 30, 31, 32,
    ];

    ubyte[32] expected = [
        25, 26, 27, 28, 29, 30, 31, 32,
        17, 18, 19, 20, 21, 22, 23, 24,
         9, 10, 11, 12, 13, 14, 15, 16,
         1,  2,  3,  4,  5,  6,  7,  8,
    ];

    expect(expected, flipY(input, 8, 4));
}

import std.datetime.systime : SysTime;

/// Gets the current system time and returns a string of it formatted using `formatTime`.
public string getTimestamp(string formatStr = "%F %T.%g", bool appendTZ = true)
{
    import std.datetime : Clock;
    SysTime curr = Clock.currTime();
    return formatTime(curr, formatStr, appendTZ);
}

/// Same as `getTimestamp`, but ensures the resulting string can exist in a file path.
///
/// See_Also:
///     - replaceInvalidPathChars
///     - getTimestamp
public string getTimestampForPath(string format = "%F %T", bool appendTZ = true, string replaceWith = "", bool squashConsecutive = true)
{
    return replaceInvalidPathChars(getTimestamp(format, appendTZ), replaceWith, squashConsecutive);
}

/// Takes a file name and returns a string equivalent to `str`, but any
/// characters that cannot exist in a file pathname will be removed or replaced.
///
/// Note that this means that path separators will be removed, since they are
/// invalid on both Windows and POSIX. Make sure the given string is only
/// a file name, not whole path.
///
/// The result is system dependent, and assumes the system is POSIX-compliant
/// if it's not Windows.
///
/// Params:
///
///     str = The string to clean up.
///
///     replaceWith = Invalid characters will be replaced with this string.
///                    Default is empty string.
///
///     squashConsecutive =  If true, consecutive invalid characters will be
///                           replaced by only one instance of `replaceWith`.
///                           Default is true.
///
public string replaceInvalidPathChars(string str, string replaceWith = "", bool squashConsecutive = true)
{
    import std.regex : regex, replaceAll, Regex, matchFirst;

    version(Windows)
    {
        auto toReplace = regex(`[><:"/\\|?*]` ~ (squashConsecutive ? "+" : ""));
    }
    else
    {
        auto toReplace = regex("[/\0]" ~ (squashConsecutive ? "+" : ""));
    }

    if (matchFirst(replaceWith, toReplace))
    {
        throw new Exception("Cannot replace invalid path characters with string \"" ~ replaceWith ~ "\" because it also contains invalid path characters.");
    }

    return replaceAll(str, toReplace, replaceWith);
}

unittest
{
    version(Windows)
    {
        string clean1 = replaceInvalidPathChars("abc??s");
        assert(clean1 == "abcs", "Received \"" ~ clean1 ~ "\".");

        string clean2 = replaceInvalidPathChars("abc??s", "_");
        assert(clean2 == "abc_s", "Received \"" ~ clean2 ~ "\".");

        string clean3 = replaceInvalidPathChars("abc??s", "_", false);
        assert(clean3 == "abc__s", "Received \"" ~ clean3 ~ "\".");

        string clean4 = replaceInvalidPathChars(`abc><:"/\|?*s`);
        assert(clean4 == "abcs", "Received \"" ~ clean4 ~ "\".");

        try
        {
            replaceInvalidPathChars("abc", ":");
            assert(false, "Expected exception to be throw on trying to replace invalid path chars with \":\".");
        }
        catch (Exception e)
        {
            // Good.
        }
    }

    version(Posix)
    {
        string clean1 = replaceInvalidPathChars("abc//s");
        assert(clean1 == "abcs", "Received \"" ~ clean1 ~ "\".");

        string clean2 = replaceInvalidPathChars("abc//s", "_");
        assert(clean2 == "abc_s", "Received \"" ~ clean2 ~ "\".");

        string clean3 = replaceInvalidPathChars("abc//s", "_", false);
        assert(clean3 == "abc__s", "Received \"" ~ clean3 ~ "\".");

        string clean4 = replaceInvalidPathChars("abc><:\"/\\|?*\0\0s");
        assert(clean4 == `abc><:"\|?*s`, "Received \"" ~ clean4 ~ "\".");

        try
        {
            replaceInvalidPathChars("abc", "/");
            assert(false, "Expected exception to be throw on trying to replace invalid path chars with \":\".");
        }
        catch (Exception e)
        {
            // Good.
        }
    }
}

/// Converts an instance of `SysTime` to a string with the following format:
/// YYYY-MM-DD HH:MM:SS TZ
/// where TZ is of the form ±HH:MM (UTC is +00:00).
///
/// Params:
///
///     time = The instance of `SysTime` to format.
///
///     formatStr = Formatting string. See datefmt docs for details.
///                  Default `%F %T.%g`.
///
///     appendTZ = If true, the resulting string will have the time zone
///                 appended at the end. Default true.
///
public string formatTime(SysTime time, string formatStr = "%F %T.%g", bool appendTZ = true)
{
    string retVal = datefmt(time, formatStr);

    if (appendTZ)
    {
        // datefmt normally outputs timezones in the format +hhmm,
        // but ISO says it should be +hh:mm, and I think that's more
        // consistent considering the timestamp is also colon-separated.
        string tz = datefmt(time, "%z");
        retVal ~= " " ~ tz[0..3] ~ ":" ~ tz[3..$];
    }

    return retVal;
}

unittest
{
    import std.datetime : DateTime, Duration, dur;
    import std.datetime.timezone : TimeZone, UTC, SimpleTimeZone;

    SysTime testUTC = SysTime(DateTime(2019, 11, 17, 20, 10, 35), dur!"msecs"(736), UTC());
    string expectedUTC = "2019-11-17 20:10:35.736 +00:00";
    string actualUTC = formatTime(testUTC);
    assert(actualUTC == expectedUTC, "Expected \"%s\", received \"%s\".".format(expectedUTC, actualUTC));

    immutable TimeZone est = new immutable SimpleTimeZone(dur!"hours"(-5), "EST");
    SysTime testEST = SysTime(DateTime(2019, 11, 17, 20, 10, 35), dur!"msecs"(736), est);
    string expectedEST = "2019-11-17 20:10:35.736 -05:00";
    string actualEST = formatTime(testEST);
    assert(actualEST == expectedEST, "Expected \"%s\", received \"%s\".".format(expectedEST, actualEST));

    immutable TimeZone cet = new immutable SimpleTimeZone(dur!"hours"(1), "CET");
    SysTime testCET = SysTime(DateTime(2019, 11, 17, 20, 10, 35), dur!"msecs"(736), cet);
    string expectCET = "2019-11-17 20:10:35.736 +01:00";
    string actualCET = formatTime(testCET);
    assert(actualCET == expectCET, "Expected \"%s\", received \"%s\".".format(expectCET, actualCET));

    immutable TimeZone npt = new immutable SimpleTimeZone(dur!"hours"(5) + dur!"minutes"(45), "NPT");
    SysTime testNPT = SysTime(DateTime(2019, 11, 17, 20, 10, 35), dur!"msecs"(736), npt);
    string expectNPT = "2019-11-17 20:10:35.736 +05:45";
    string actualNPT = formatTime(testNPT);
    assert(actualNPT == expectNPT, "Expected \"%s\", received \"%s\".".format(expectNPT, actualNPT));
}
