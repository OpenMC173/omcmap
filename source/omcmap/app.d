module omcmap.app;

import std.algorithm.searching;
import std.conv;
import std.file;
import std.getopt;
import std.path;
import std.stdio;
import std.string;
import std.uni;

import omcmap.render;
import omcmap.util;

import imageformats;


void main(string[] args)
{
	GetoptResult options;
	string outputFolderPath;
	Mode mode = Mode.normal;
	bool usingDefaultOutputDir = false;
	bool ignoreCache = false;

	try
	{
		options = getopt(
			args,

			"output",
			"Folder in which to save rendered map.",
			&outputFolderPath,

			"mode",
			"Select map rendering mode. Default is \"normal\".\n" ~
			"              normal           2D overhead view of the world.\n" ~
			"              heightmap        Greyscale elevation map (from black at the bottom, white at the top). Ignores water.\n" ~
			"              heightmap-color  Colored elevation map (from bottom to top: black->violet->blue->green->yellow->red->brown->white). Ignores water.\n" ~
			"              blocklight       Greyscale map showing blocklight level of top-most blocks.",
			&mode,

			"no-cache",
			"Forces all regions to be rendered even if some can be read from the cache. Caches will be computed regardless.",
			&ignoreCache,
		);
	}
	catch (GetOptException e)
	{
		writeln(e.message);
		return printHelp(options);
	}
	catch (ConvException e)
	{
		if (e.message.startsWith("Mode does not have a member named "))
		{
			writeln("Invalid mode \"" ~ e.message[e.message.indexOf('\'') + 1 .. e.message.lastIndexOf('\'')] ~ "\".");
			writeln("Valid modes are \"normal\" (default), \"heightmap\", and \"blocklight\".");
			return;
		}
		else throw e;
	}

	if (args.length < 2 || args.length > 5 || options.helpWanted) return printHelp(options);

	string worldFolderPath = args[1];

	if (!validateDirectory(worldFolderPath)) return;

	string worldName = baseName(worldFolderPath);

	if (outputFolderPath == string.init)
	{
		outputFolderPath = chainPath(getcwd(), worldName).to!string;
		usingDefaultOutputDir = true;
	}

	if (!validateDirectory(outputFolderPath))
	{
		if (!isValidPath(outputFolderPath)) return;

		string choice;
		do
		{
			write("Create folder? [y/n] ");
			readf("%s\n", choice);
			choice = toLower(choice);
		}
		while (choice != "y" && choice != "n");

		if (choice == "y")
		{
			writeln("Creating folder...");
			mkdir(outputFolderPath);
		}
		else
		{
			writeln("Exiting");
			return;
		}

	}
	else if (exists(outputFolderPath) && usingDefaultOutputDir)
	{
		writeln("Default output folder already exists:");
		writeln("\t" ~ outputFolderPath);
		string choice;
		do
		{
			write("Are you sure you want to use this folder (files may be overwritten)? [y/n] ");
			readf("%s\n", choice);
			choice = toLower(choice);
		}
		while (choice != "y" && choice != "n");

		if (choice == "n")
		{
			writeln("You may specify an exact path by running omcmap with the \"--output\" option.");
			writeln("Exiting.");
			return;
		}
	}

	writeln("Rendering world from folder:");
    writeln("\t" ~ absolutePath(worldFolderPath));

	writeln("Map will be saved to:");
	writeln("\t" ~ absolutePath(outputFolderPath));

	WorldFormat vanillaWorldFormat = WorldFormat("region", 32, 16, 16);

	WorldRenderer renderer = new WorldRenderer(worldFolderPath, outputFolderPath.buildPath("cache-" ~ mode.to!string), vanillaWorldFormat);

	WorldRenderer.RenderResult result = renderer.render(mode, ignoreCache);

	string fileName = "world-" ~ mode.to!string;

    immutable string outputFile = buildPath(
        outputFolderPath,
        fileName ~ " " ~ getTimestampForPath("%F %T", false) ~ ".png"
    );

    write_png(outputFile, result.image.w, result.image.h, result.image.pixels);
}

void printHelp(GetoptResult options)
{
	defaultGetoptPrinter(
"
Usage:
omcmap --help
omcmap [options] <World Directory> [options]
",
		options.options);
	writeln();
}
