module omcmap.render;

import std.array;
import std.base64;
import std.bitmanip;
import std.conv;
import std.digest.murmurhash;
import std.exception;
import std.file;
import std.path;
import std.range;
import std.stdio;
import std.string;
import std.zlib;

import imageformats;
import imageformats.png;
import omcnbt;

import omcmap.block;
import omcmap.color;
import omcmap.test;
import omcmap.util;


enum SECTOR_SIZE = 4096;

enum Mode
{
    normal,
    heightmap,
    heightmap_color,
    blocklight,
}

struct PositionedImage
{
    IFImage image;
    int x;
    int y;
}

struct WorldFormat
{
    string dimension0regionsFolder;
    uint regionWidth;
    uint chunkWidth;
    uint chunkHeight;

    uint chunkArea()
    {
        return chunkWidth ^^ 2;
    }

    uint chunkVolume()
    {
        return chunkArea * chunkVolume;
    }

    uint chunksPerRegion()
    {
        return regionWidth ^^ 2;
    }
}


class WorldRenderer
{
    class RenderResult
    {
        IFImage image;
        PositionedImage[] regionImages;
        string[string] hashes;
    }

    private string worldFolderPath;
    private string cacheImagesPath;
    private string cacheHashesPath;
    private WorldFormat worldFormat;

    private IFImage heightmapColors;

    this(string worldFolderPath, string cacheImagesPath, string cacheHashesPath, WorldFormat worldFormat)
    {
        this.worldFolderPath = worldFolderPath;
        this.worldFormat = worldFormat;
        this.cacheImagesPath = cacheImagesPath;
        this.cacheHashesPath = cacheHashesPath;

        this.heightmapColors = thisExePath.dirName.buildPath("heightmap-colors.png").read_png();
        assert(heightmapColors.c == ColFmt.RGBA, "Expected heightmap-colors.png to be in RGBA format.");
    }

    RenderResult render(Mode mode, bool ignoreCache)
    in { assert(exists(worldFolderPath) && isDir(worldFolderPath)); }
    do {
        immutable string regionInputPath = buildPath(worldFolderPath, worldFormat.dimension0regionsFolder);
        if (!validateDirectory(regionInputPath))
        {
            throw new Exception("Region folder at \"" ~ regionInputPath ~ "\" is invalid.");
        }

        if (!exists(cacheImagesPath) || !isDir(cacheImagesPath))
        {
            mkdir(cacheImagesPath);
        }

        DirEntry[] regionFiles = dirEntries(regionInputPath, "*.mcr", SpanMode.shallow, false).array;

        // Useful for making the log outputs nice and aligned.
        immutable string digits = regionFiles.length.to!string.length.to!string;

        PositionedImage[] images;

        uint current = 0;
        uint numRendered = 0;
        uint numFromCache = 0;

        foreach (DirEntry file; regionFiles)
        {
            if (!file.isFile()) continue;

            string[] nameParts = file.name().baseName().stripExtension().split('.');
            immutable int regionX = nameParts[1].to!int;
            immutable int regionY = nameParts[2].to!int;

            string progress = ("Processing region %" ~ digits ~ "d / %d (" ~ file.name.baseName() ~ ")...")
                .format(current + 1, regionFiles.length)
                .leftJustify(50);

            write("\r" ~ progress);

            IFImage image = IFImage(
                worldFormat.chunkWidth * worldFormat.regionWidth,
                worldFormat.chunkWidth * worldFormat.regionWidth,
                ColFmt.RGBA,
                new ubyte[((worldFormat.chunkWidth * worldFormat.regionWidth) ^^ 2) * RGBA.sizeof],
            );

            renderRegionFile(file, image, mode, ignoreCache);
            images ~= PositionedImage(image, regionX, regionY);
            current++;
        }
        writeln();
        writefln("Rendered %d regions, read %d regions from cache.", numRendered, numFromCache);

        WorldRenderer.RenderResult result = new WorldRenderer.RenderResult();
        result.image = stitchImages(images);
        result.regionImages = images;

        return result;
    }

    struct HeaderChunkInfo
    {
        bool exists;
        uint dataOffset;
        ubyte numSectors;
    }

    void renderRegionFile(DirEntry file, ref IFImage result, Mode mode, bool ignoreCache)
    in { assert(file.isFile()); }
    do {


        HeaderChunkInfo[][] headerChunkInfos;
        headerChunkInfos.length = worldFormat.regionWidth;
        foreach (ref HeaderChunkInfo[] headerChunkInfosInner; headerChunkInfos)
        {
            headerChunkInfosInner.length = worldFormat.regionWidth;
        }

        immutable string fileNameNoExt = file.name().baseName().stripExtension();

        ubyte[] contents = cast(ubyte[])read(file.name);
        // Using MurmurHash because it's supposedly one of the faster hashes:
        // https://softwareengineering.stackexchange.com/a/145633
        // At any rate, it's certainly fast enough. Might be worth trying something
        // more mainstream like MD5 or CRC if users want to manually mess with the
        // cache files...
        string hashOfCurr = hexDigest!(MurmurHash3!(128, 64))(contents).to!string;

        immutable string cacheFilePath = cacheHashesPath.buildPath(fileNameNoExt ~ ".cache.txt");

        if (!ignoreCache && exists(cacheFilePath))
        {
            if (DirEntry(cacheFilePath).isFile)
            {
                string hashOfPrev = readText(cacheFilePath);
                if (hashOfCurr == hashOfPrev)
                {
                    immutable string outputFilePath = cacheImagesPath.buildPath(fileNameNoExt ~ ".png");

                    if (exists(outputFilePath))
                    {
                        IFImage cachedImg = read_png(outputFilePath);
                        if (cachedImg.w != result.w || cachedImg.h != result.h)
                        {
                            writefln("\nError: Dimensions of cached image \"%s\" are incorrect, will re-render.", outputFilePath);
                            std.file.write(cacheFilePath, hashOfCurr);
                        }
                        else
                        {
                            result = cachedImg;
                            return;
                        }
                    }
                    else
                    {
                        writefln("\nWarning: Cached image \"%s\" not found, will re-render.", outputFilePath);
                        std.file.write(cacheFilePath, hashOfCurr);
                    }
                }
                else
                {
                    std.file.write(cacheFilePath, hashOfCurr);
                }
            }
            else
            {
                writefln("\nError: \"%s\" is not a file - cannot read the cache of the current region.", cacheFilePath);
            }
        }
        else
        {
            std.file.write(cacheFilePath, hashOfCurr);
        }

        for (ubyte x = 0; x < worldFormat.regionWidth; x++)
        {
            for (ubyte z = 0; z < worldFormat.regionWidth; z++)
            {
                HeaderChunkInfo* current = &(headerChunkInfos[x][z]);
                immutable uint offsetInContents = 4 * (x + 32 * z);
                ubyte[4] field1arr = [cast(ubyte)0] ~ contents[offsetInContents .. offsetInContents + 3];
                current.dataOffset = bigEndianToNative!uint(field1arr) * SECTOR_SIZE;
                current.numSectors = contents[offsetInContents + 3];
                current.exists = current.dataOffset != 0 && current.numSectors != 0;
            }
        }

        uint chunksProcessed = 0;
        uint chunksRendered = 0;
        uint chunksSkipped = 0;
        for (ubyte x = 0; x < worldFormat.regionWidth; x++)
        {
            for (ubyte z = 0; z < worldFormat.regionWidth; z++)
            {
                HeaderChunkInfo* current = &(headerChunkInfos[x][z]);
                chunksProcessed++;

                if (current.exists)
                {
                    chunksRendered++;
                }
                else
                {
                    chunksSkipped++;
                }

                //writef("\r\tChunks rendered: %4d   skipped: %4d   total: %4d / %4d", chunksRendered, chunksSkipped, chunksProcessed, CHUNKS_PER_REGION);

                if (!current.exists)
                {
                    continue;
                }

                ubyte[4] sizeArr = contents[current.dataOffset .. current.dataOffset + 4];
                immutable uint size = bigEndianToNative!uint(sizeArr);
                immutable ubyte compressionType = contents[current.dataOffset + 4]; // Should be 2 in Beta 1.7.3
                immutable uint compressedDataStart = current.dataOffset + 5;
                immutable uint compressedDataEnd = current.dataOffset + 5 + size - 1;
                ubyte[] compressedData = contents[compressedDataStart ..  compressedDataEnd];

                if (compressionType == 1) {
                    writeln("Chunk data is compressed using GZip, not yet supported.");
                    continue;
                }

                enforce(compressionType == 2, "Unsupported compression type ID: " ~ compressionType.to!string());

                ubyte[] data = cast(ubyte[])uncompress(cast(void[])compressedData);

                CompoundTag rootTag;
                try
                {
                    rootTag = parseNBT!CompoundTag(data);
                    renderChunk(rootTag, result, x, z, mode);
                }
                catch (Exception e)
                {
                    import std.file : write;
                    write("./bad_nbt.nbt", data);
                    writeln();
                    writefln("Encountered error while parsing NBT of chunk %d at chunk coordinates [ %d, %d ]:", chunksProcessed, x, z);
                    writeln(e.toString());
                    writeln("\nCause: ");
                    writeln(e.next().toString());
                    return;
                }
            }
        }
    }
}



private void renderChunk(CompoundTag rootTag, ref IFImage dest, ubyte localX, ubyte localZ, Mode mode)
in
{
    assert(localX < REGION_LENGTH);
    assert(localZ < REGION_LENGTH);
}
do
{
    RGBA[CHUNK_AREA] colors;

    immutable uint destX = localX * CHUNK_WIDTH;
    immutable uint destY = localZ * CHUNK_WIDTH;

    CompoundTag levelTag = rootTag.getChild!CompoundTag("Level");

    //writeln();
    //writefln("Rendering chunk at world coords (%d, %d)", levelTag.getChild!IntTag("xPos").data * CHUNK_WIDTH, levelTag.getChild!IntTag("zPos").data * CHUNK_WIDTH);

    //ubyte[256] heightMap = cast(ubyte[])levelTag.getChild!ByteArrayTag("HeightMap").data;
    ubyte[CHUNK_VOLUME] blocks = cast(ubyte[]) levelTag.getChild!ByteArrayTag("Blocks").data;
    ubyte[CHUNK_VOLUME / 2] blockData = cast(ubyte[]) levelTag.getChild!ByteArrayTag("Data").data;
    ubyte[CHUNK_VOLUME / 2] blockLight = cast(ubyte[]) levelTag.getChild!ByteArrayTag("BlockLight").data;

    for (ubyte x = 0; x < CHUNK_WIDTH; x++)
    {
        for (ubyte z = 0; z < CHUNK_WIDTH; z++)
        {
            immutable uint localDestX = x;
            immutable uint localDestY = (CHUNK_WIDTH - 1) - z;
            immutable uint destIndex = localDestX + (CHUNK_WIDTH * localDestY);

            int nextYtoCheck = 127;
            // int nextYtoCheck = heightMap[z * CHUNK_WIDTH + x] - 1;
            // // heightMap records the lowest Y at which skyLight is max,
            // // so if there is a block at y = 128 then there is no actual
            // // air block where light level is max.
            // if (nextYtoCheck == 128) nextYtoCheck--;

            size_t nextIndex = nextYtoCheck + (z * CHUNK_HEIGHT + (x * CHUNK_HEIGHT * CHUNK_WIDTH));
            ubyte blockID = blocks[nextIndex];
            // Continue as long as:
            // We're above 0
            // AND
            // The current block is transparent, or, if we're in heightmap mode, it's water.
            while ((transparentBlocks[blockID] ||
                    (
                        (mode == Mode.heightmap || mode == Mode.heightmap_color) &&
                        (blockID == Blocks.Water_Still || blockID == Blocks.Water_Flowing)
                    )
                   ) && nextYtoCheck >= 0)
            {
                nextYtoCheck--;
                nextIndex = nextYtoCheck + (z * CHUNK_HEIGHT + (x * CHUNK_HEIGHT * CHUNK_WIDTH));
                blockID = blocks[nextIndex];
            }

            if (mode == Mode.heightmap)
            {
                ubyte shade = (nextYtoCheck * 2).to!ubyte;
                colors[destIndex] = RGBA(shade, shade, shade, 255);
                continue;
            }
            else if (mode == Mode.heightmap_color)
            {
                ubyte[4] shadeBytes = heightmapColors.pixels[(4 * nextYtoCheck) .. ((4 * nextYtoCheck) + 4)];
                colors[destIndex] = RGBA(shadeBytes[0], shadeBytes[1], shadeBytes[2], shadeBytes[3]);
                continue;
            }
            else if (mode == Mode.blocklight)
            {
                // For block light, we want to get the light level of the block
                // *above* the surface block, that is, the last air block. So we
                // subtract one from the index... unless it's y = 127, where
                // block light is always 0 (since there's no block at y = 128).
                if (nextYtoCheck == 127)
                {
                    colors[destIndex] = RGBA(0, 0, 0, 255);
                    continue;
                }
                nextIndex++;
                ubyte lightLevel = blockLight[nextIndex / 2];
                // Least significant bits correspond to first byte in Blocks
                if (nextIndex % 2 != 0)
                {
                    lightLevel = (lightLevel & 0b11110000) >> 4;
                }
                else
                {
                    lightLevel = lightLevel & 0b00001111;
                }
                ubyte shade = (lightLevel * 16).to!ubyte;
                colors[destIndex] = RGBA(shade.to!ubyte, shade.to!ubyte, shade.to!ubyte, 255);
                continue;
            }

            RGBA color;
            if (hasSubTypes[blockID])
            {
                ubyte data = blockData[nextIndex / 2];

                // Least significant bits correspond to first byte in Blocks
                if (nextIndex % 2 != 0)
                {
                    data = (data & 0b11110000) >> 4;
                }
                else
                {
                    data = data & 0b00001111;
                }
                color = subTypeColors[blockID][data];
            }
            else
            {
                color = blockColors[blockID];
            }

            colors[localDestX + (CHUNK_WIDTH * localDestY)] = color;
        }
    }

    ubyte[CHUNK_AREA * RGBA.sizeof] colorBytes = colorsToBytes(colors).flipY(CHUNK_WIDTH * 4, CHUNK_WIDTH);

    int srcY = 0;
    //writefln("Filling in region of image:\n\tleft: %d\n\tright: %d\n\ttop: %d\n\tbottom: %d", destX, destX + CHUNK_WIDTH, destY, destY + CHUNK_WIDTH);
    for (int y = destY; y < destY + CHUNK_WIDTH; y++)
    {
        immutable uint destIdx = (destX * 4) + (dest.w * 4 * y);
        immutable uint srcIdx = (srcY * (4 * CHUNK_WIDTH));
        //writefln("Copying colorBytes[%d .. %d] to dest.pixels[%d .. %d]", srcIdx, srcIdx + (4 * CHUNK_WIDTH), destIdx, destIdx + (CHUNK_WIDTH * 4));
        dest.pixels[destIdx .. destIdx + (CHUNK_WIDTH * 4)] = colorBytes[srcIdx .. srcIdx + (4 * CHUNK_WIDTH)];

        /*writeln("moving to next row");
        for (int x = destX; x < destX + CHUNK_WIDTH; x += 4)
        {
            for (ubyte i = 0; i < 4; i++)
            {
                writefln("Writing from colorBytes[%d] to dest.pixels[%d]", i + srcX + (CHUNK_WIDTH * srcY), i + x + (dest.w * y));
                dest.pixels[i + x + (dest.w * y)] = colorBytes[i + srcX + (CHUNK_WIDTH * srcY)];
            }
            srcX += 4;
        }*/
        srcY++;
    }
}


private IFImage stitchImages(PositionedImage[] images)
in
{
    const PositionedImage first = images[0];
    immutable uint expectedWidth = first.image.w;
    immutable uint expectedHeight = first.image.h;

    foreach (pimg; images)
    {
        expect(expectedWidth, pimg.image.w);
        expect(expectedHeight, pimg.image.h);
    }
}
do
{
    writeln("Stitching region images together...");

    immutable uint pieceWidth = images[0].image.w;
    immutable uint pieceHeight = images[0].image.h;

    int minX = images[0].x;
    int maxX = images[0].x;
    int minY = images[0].y;
    int maxY = images[0].y;

    foreach (pimg; images)
    {
        if (pimg.x < minX) minX = pimg.x;
        else if (pimg.x > maxX) maxX = pimg.x;

        if (pimg.y < minY) minY = pimg.y;
        else if (pimg.y > maxY) maxY = pimg.y;
    }

    immutable int spanX = maxX - minX + 1;
    immutable int spanY = (maxY - minY + 1);
    immutable int width = spanX * pieceWidth;
    immutable int height = spanY * pieceHeight;

    IFImage[][] imageGrid = new IFImage[][](spanY, spanX);

    foreach (pimg; images)
    {
        imageGrid[pimg.y + -minY][pimg.x + -minX] = pimg.image;
    }

    IFImage image = IFImage(width, height, ColFmt.RGBA, new ubyte[width * height * 4]);

    uint row = 0;

    foreach (rowOfPieces; imageGrid)
    {
        uint col = 0;

        foreach (piece; rowOfPieces)
        {
            if (piece == IFImage.init)
            {
                col++;
                continue;
            }

            for (uint y = 0; y < pieceHeight; y++)
            {
                immutable uint srcRowStartIdx = y * 4 * pieceWidth;
                immutable uint destPieceRowStartIdx = (((row * pieceHeight) + y) * 4 * width) + (col * pieceWidth * 4);
                image.pixels[destPieceRowStartIdx .. destPieceRowStartIdx + 4 * pieceWidth] = piece.pixels[srcRowStartIdx .. srcRowStartIdx + 4 * pieceWidth];
            }

            col++;
        }

        row++;
    }

    return image;
}
