module omcmap.block;

import omcmap.color;

enum Blocks {
    Air = 0,
    Stone,
    Grass,
    Dirt,
    Cobblestone,
    Planks,
    Sapling,
    Bedrock,
    Water_Still,
    Water_Flowing,
    Lava_Still,
    Lava_Flow,
    Sand,
    Gravel,
    Gold_Ore,
    Iron_Ore,
    Coal_Ore,
    Log,
    Leaves,
    Sponge,
    Glass,
    Lapis_Ore,
    Lapis_Block,
    Dispenser,
    Sandstone,
    Note_Block,
    Bed,
    Powered_Rail,
    Detector_Rail,
    Sticky_Piston,
    Cobweb,
    Tall_Grass,
    Dead_Shrub, // ?
    Piston,
    Piston_Head,
    Wool,
    End_Portal, // Not used in this version?,
    Dandelion,
    Rose,
    Brown_Mushroom,
    Red_Mushroom,
    Gold_Block,
    Iron_Block,
    Double_Slab,
    Single_Slab,
    Bricks,
    TNT,
    Bookshelf,
    Moss_Stone,
    Obsidian,
    Torch,
    Fire,
    Mob_Spawner,
    Wooden_Stairs,
    Chest,
    Redstone_Wire,
    Diamond_Ore,
    Diamond_Block,
    Crafting_Table,
    Wheat_Crop,
    Farmland,
    Furnace_off,
    Furnace_on,
    Sign_on_ground,
    Door,
    Ladder,
    Rail,
    Cobblestone_Stairs,
    Sign_on_wall,
    Lever,
    Stone_Pressure_Plate,
    Iron_Door,
    Wooden_Pressure_Plate,
    Redstone_Ore_dim,
    Redstone_Ore_glowing,
    Redstone_Torch_off,
    Redstone_Torch_on,
    Button,
    Snow_Layer,
    Ice,
    Snow_Block,
    Cactus,
    Clay,
    Sugar_Cane,
    Jukebox,
    Fence,
    Pumpkin,
    Netherrack,
    Soul_Sand,
    Glowstone,
    Portal,
    Jack_o_Lantern,
    Cake,
    Repeater_off,
    Repeater_on,
    April_Fools_Chest,
    Trapdoor,
}

public bool[ubyte.max] transparentBlocks = [
    Blocks.Air: true,
    Blocks.Sapling: true,
    Blocks.Glass: true,
    Blocks.Powered_Rail: true,
    Blocks.Detector_Rail: true,
    Blocks.Brown_Mushroom: true,
    Blocks.Red_Mushroom: true,
    Blocks.Torch: true,
    Blocks.Redstone_Wire: true,
    Blocks.Wheat_Crop: true,
    Blocks.Sign_on_ground: true,
    Blocks.Ladder: true,
    Blocks.Rail: true,
    Blocks.Sign_on_wall: true,
    Blocks.Redstone_Torch_off: true,
    Blocks.Redstone_Torch_on: true,
    Blocks.Button: true,
    Blocks.Fence: true,
    Blocks.Portal: true,
    Blocks.Repeater_off: true,
    Blocks.Repeater_on: true,
];

public bool[ubyte.max] hasSubTypes = [
    Blocks.Log: true,
    Blocks.Leaves: true,
    Blocks.Wool: true,
    Blocks.Double_Slab: true,
    Blocks.Single_Slab: true,
];

public RGBA[16][ubyte.max] subTypeColors;

static this()
{
    subTypeColors[Blocks.Log][0] = RGBA(106, 84, 51, 255);      // Oak
    subTypeColors[Blocks.Log][1] = RGBA(50, 32, 14, 255);       // Spruce
    subTypeColors[Blocks.Log][2] = RGBA(217, 217, 212, 255);    // Birch

    subTypeColors[Blocks.Leaves][0] = RGBA(98, 142, 51, 255);   // Oak
    subTypeColors[Blocks.Leaves][1] = RGBA(97, 153, 97, 255);   // Spruce
    subTypeColors[Blocks.Leaves][2] = RGBA(128, 167, 85, 255);  // Birch

    // if bit 0b_1000 is set, leaves will be checked for decay.
    // In future versions, there is also a "no decay" bit for player-placed leaves.
    subTypeColors[Blocks.Leaves][8] = RGBA(98, 142, 51, 255);   // Oak (check decay)
    subTypeColors[Blocks.Leaves][9] = RGBA(97, 153, 97, 255);   // Spruce (check decay)
    subTypeColors[Blocks.Leaves][10] = RGBA(128, 167, 85, 255); // Birch (check decay)

    subTypeColors[Blocks.Wool][0] = RGBA(235, 235, 235, 255);   // White
    subTypeColors[Blocks.Wool][1] = RGBA(234, 128, 57, 255);    // Orange
    subTypeColors[Blocks.Wool][2] = RGBA(191, 78, 201, 255);    // Magenta
    subTypeColors[Blocks.Wool][3] = RGBA(107, 140, 212, 255);   // Light Blue
    subTypeColors[Blocks.Wool][4] = RGBA(195, 182, 29, 255);    // Yellow
    subTypeColors[Blocks.Wool][5] = RGBA(60, 189, 48, 255);     // Lime Green
    subTypeColors[Blocks.Wool][6] = RGBA(218, 134, 156, 255);   // Pink
    subTypeColors[Blocks.Wool][7] = RGBA(67, 67, 67, 255);      // Dark Grey
    subTypeColors[Blocks.Wool][8] = RGBA(159, 166, 166, 255);   // Light Grey
    subTypeColors[Blocks.Wool][9] = RGBA(39, 117, 150, 255);    // Cyan
    subTypeColors[Blocks.Wool][10] = RGBA(130, 55, 196, 255);   // Purple
    subTypeColors[Blocks.Wool][11] = RGBA(39, 51, 155, 255);    // Dark Blue
    subTypeColors[Blocks.Wool][12] = RGBA(96, 51, 28, 255);     // Brown
    subTypeColors[Blocks.Wool][13] = RGBA(56, 77, 24, 255);     // Dark Green
    subTypeColors[Blocks.Wool][14] = RGBA(165, 45, 41, 255);    // Red
    subTypeColors[Blocks.Wool][15] = RGBA(27, 24, 24, 255);     // Black

    subTypeColors[Blocks.Double_Slab][0] = RGBA(168, 168, 168, 255);    // Stone
    subTypeColors[Blocks.Double_Slab][1] = RGBA(218, 210, 159, 255);    // Sandstone
    subTypeColors[Blocks.Double_Slab][2] = RGBA(183, 148, 94, 255);     // Planks
    subTypeColors[Blocks.Double_Slab][3] = RGBA(110, 110, 110, 255);    // Cobblestone

    subTypeColors[Blocks.Single_Slab][0] = RGBA(168, 168, 168, 255);    // Stone
    subTypeColors[Blocks.Single_Slab][1] = RGBA(218, 210, 159, 255);    // Sandstone
    subTypeColors[Blocks.Single_Slab][2] = RGBA(183, 148, 94, 255);     // Planks
    subTypeColors[Blocks.Single_Slab][3] = RGBA(110, 110, 110, 255);    // Cobblestone

}

public RGBA[ubyte.max] blockColors = [

    0: RGBA(0, 0, 0, 0),                   // Air
    1: RGBA(126, 126, 126, 255),           // Stone
    2: RGBA(122, 177, 77, 255),            // Grass
    3: RGBA(137,  98, 69, 255),            // Dirt
    4: RGBA(110, 110, 110, 255),           // Cobblestone
    5: RGBA(183, 148, 94, 255),            // Planks
    6: RGBA(73, 204, 37, 255),             // Sapling
    7: RGBA(75, 75, 75, 255),              // Bedrock
    8: RGBA(45, 96, 255, 255),             // Water Still
    9: RGBA(45, 96, 255, 255),             // Water Flowing

    10: RGBA(246, 115, 0, 255),             // Lava Still
    11: RGBA(246, 115, 0, 255),             // Lava Flow
    12: RGBA(219, 211, 160, 255),           // Sand
    13: RGBA(142, 133, 132, 255),           // Gravel
    14: RGBA(248, 175, 43, 255),            // Gold Ore
    15: RGBA(216, 175, 147, 255),           // Iron Ore
    16: RGBA(63, 63, 63, 255),              // Coal Ore
    17: RGBA(106, 84, 51, 255),             // Log
    18: RGBA(98, 142, 51, 255),             // Leaves
    19: RGBA(203, 203, 68, 255),            // Sponge

    20: RGBA(192, 245, 254, 128),           // Glass
    21: RGBA(30, 72, 168, 255),             // Lapis Ore
    22: RGBA(23, 66, 206, 255),             // Lapis Block
    23: RGBA(90, 90, 90, 255),              // Dispenser
    24: RGBA(218, 210, 159, 255),           // Sandstone
    25: RGBA(120, 77, 55, 255),             // Note Block
    26: RGBA(144, 28, 29, 255),             // Bed
    27: RGBA(0, 0, 0, 0),                   // Powered Rail
    28: RGBA(0, 0, 0, 0),                   // Detector Rail
    29: RGBA(130, 152, 94, 255),            // Sticky Piston

    30: RGBA(221, 221, 221, 255),           // Cobweb
    31: RGBA(116, 164, 74, 255),            // Tall Grass
    32: RGBA(148, 100, 40, 255),            // Dead Shrub duplicate?
    33: RGBA(149, 121, 73, 255),            // Piston
    34: RGBA(149, 121, 73, 255),            // Piston Head
    35: RGBA(235, 235, 235, 255),           // Wool
                                            // Unknown (End portal in later versions)
    37: RGBA(122, 177, 77, 255),            // Dandelion
    38: RGBA(122, 177, 77, 255),            // Rose
    39: RGBA(0, 0, 0, 0),                   // Brown Mushroom

    40: RGBA(0, 0, 0, 0),                   // Red Mushroom
    41: RGBA(252, 240, 79, 255),            // Gold Block
    42: RGBA(222, 222, 222, 255),           // Iron Block
    43: RGBA(168, 168, 168, 255),           // Double Slab
    44: RGBA(168, 168, 168, 255),           // Single Slab
    45: RGBA(143, 79, 62, 255),             // Bricks
    46: RGBA(193, 60, 23, 255),             // TNT
    47: RGBA(183, 148, 94, 255),            // Bookshelf
    48: RGBA(106, 116, 106, 255),           // Moss Stone
    49: RGBA(25, 21, 37, 255),              // Obsidian

    50: RGBA(0, 0, 0, 0),                   // Torch
    51: RGBA(255, 128, 0, 255),             // Fire
    52: RGBA(27, 84, 124, 255),             // Mob Spawner
    53: RGBA(183, 148, 94, 255),            // Wooden Stairs
    54: RGBA(158, 110, 37, 255),            // Chest
    55: RGBA(170, 0, 0, 0),                 // Redstone Wire
    56: RGBA(93, 236, 245, 255),            // Diamond Ore
    57: RGBA(121, 222, 217, 255),           // Diamond Block
    58: RGBA(138, 90, 54, 255),             // Crafting Table
    59: RGBA(0, 0, 0, 0),                   // Wheat Crop

    60: RGBA(99, 54, 19, 255),              // Farmland
    61: RGBA(90, 90, 90, 255),              // Furnace (off)
    62: RGBA(90, 90, 90, 255),              // Furnace (on)
    63: RGBA(183, 148, 94, 0),              // Sign (on ground)
    64: RGBA(141, 109, 55, 255),            // Door
    65: RGBA(0, 0, 0, 0),                   // Ladder
    66: RGBA(0, 0, 0, 0),                   // Rail
    67: RGBA(110, 110, 110, 255),           // Cobblestone Stairs
    68: RGBA(183, 148, 94, 0),              // Sign (on wall)
    69: RGBA(124, 98, 62, 255),             // Lever

    70: RGBA(126, 126, 126, 255),           // Stone Pressure Plate
    71: RGBA(168, 168, 168, 255),           // Iron Door
    72: RGBA(183, 148, 94, 255),            // Wooden Pressure Plate
    73: RGBA(138, 116, 116, 255),           // Redstone Ore dim (?)
    74: RGBA(138, 116, 116, 255),           // Redstone Ore glowing (?)
    75: RGBA(0, 0, 0, 0),                   // Redstone Torch (off)
    76: RGBA(0, 0, 0, 0),                   // Redstone Torch (on)
    77: RGBA(0, 0, 0, 0),                   // Button
    78: RGBA(241, 252, 252, 255),           // Snow Layer
    79: RGBA(131, 175, 255, 255),           // Ice

    80: RGBA(241, 252, 252, 255),           // Snow Block
    81: RGBA(14, 114, 27, 255),             // Cactus
    82: RGBA(159, 165, 177, 255),           // Clay
    83: RGBA(170, 219, 116, 255),           // Sugar Cane
    84: RGBA(120, 77, 55, 255),             // Jukebox
    85: RGBA(0, 0, 0, 0),                   // Fence
    86: RGBA(204, 127, 30, 255),            // Pumpkin
    87: RGBA(125, 68, 65, 255),             // Netherrack
    88: RGBA(88, 68, 56, 255),              // Soul Sand
    89: RGBA(255, 188, 94, 255),            // Glowstone

    90: RGBA(0, 0, 0, 0),                   // Portal
    91: RGBA(204, 127, 30, 255),            // Jack-o-Lantern
    92: RGBA(230, 188, 188, 255),           // Cake
    93: RGBA(0, 0, 0, 0),                   // Repeater (off)
    94: RGBA(0, 0, 0, 0),                   // Repeater (on)
    95: RGBA(158, 110, 37, 255),            // April Fools' Chest
    96: RGBA(141, 109, 55, 255),            // Trapdoor
];

