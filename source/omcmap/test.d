module omcmap.test;

import std.conv : to;
import std.stdio;
import std.traits;


public void expect(E, A)(const ref E expected, const ref A actual)
if (!isDynamicArray!E && !isDynamicArray!A)
{
    import core.exception : AssertError;

    try
    {
        assert(actual == expected);
    }
    catch (AssertError e)
    {
        writeln("Assertion failed. Expected vs. Actual:\n");
        writeln("----------------------------------------");
        writeln(expected.to!string);
        writeln("----------------------------------------");
        writeln(actual.to!string);
        writeln("----------------------------------------");
        throw e;
    }
}

public void expect(T)(const T expected, const T actual)
if (isDynamicArray!T || isStaticArray!T)
{
    import core.exception : AssertError;
    import std.algorithm.comparison : equal;

    try
    {
        import std.range.primitives : ElementType;

        const (ElementType!T)[] e = expected[].to!((ElementType!T)[]);
        const (ElementType!T)[] a = actual[].to!((ElementType!T)[]);
        assert(equal(e, a));
    }
    catch (AssertError e)
    {
        writeln("Assertion failed. Expected vs. Actual:\n");
        writeln("----------------------------------------");
        writeln(expected.to!string);
        writeln("----------------------------------------");
        writeln(actual.to!string);
        writeln("----------------------------------------");
        throw e;
    }
}
