CHANGELOG
=========

v1.4.0
------

*2020-05-03*

- **FEATURE** - A new mode is available: `heightmap-color`. It's similar to `heightmap`, but the output image is based on a "heatmap"-like scale, where lower elevations are black/purple/blue, and higher elevations are red/brown/white. The colors are taken from the `heightmap-colors.png`. Colors may be customized by editing that file.
- **CHANGE** - Final render PNGs contain the timestamp in the file name, so old renders don't get overwritten.
- **CHANGE** - Confirmation messages now print full paths.
- **BUGFIX** - Generated maps are no longer just colored noise on affected platforms (e.g. Linux).


v1.3.0
------

*2019-08-04*

- **FEATURE** – omcmap now saves a MurmurHash3 digest of a region file when rendering a world. Next time the world is rendered, each region's saved digest is compared compared against an up-to-date digest. If the two match (for a given region), that means that region hasn't changed, and the existing image from the `regions` sub-folder is used instead. Note that a region's hash depends not only on the blocks themselves, but also on the positions of entities in the region and so forth. The current implementation will therefore encounter a lot of false positives, but it's still something. Making the caching smarter is something for another day.

v1.2.0
------

*2019-07-07*

- **CHANGE** – `--heightmap` no longer works, use `--mode=heightmap` instead.
- **CHANGE** – User is asked whether default output folder should be used if it already exists.
- **CHANGE** – More useful messages in terminal overall.
- **FEATURE** – `blockLight` mode which renders the block light level (from torches etc, not from sunlight) at surface.

v1.1.0
------

*2019-06-24*

- **FEATURE** – `--heightmap` mode, which renders the world as a greyscale
heightmap.
- **BUGFIX** – specifying `--output` now works.


v1.0.0
------

*2019-02-21*

Initial Release
