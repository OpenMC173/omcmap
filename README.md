# omcmap

omcmap is a command-line tool for generating 2D overhead maps of Minecraft
worlds saved in Minecraft Beta 1.3 to 1.7.3.

The program should be run from the command line as follows:

Linux:
```bash
./omcmap [options] <path>
```

Windows:
```powershell
.\omcmap.exe [options] <path>
```

`<path>` should be a path to a folder containing a `level.dat` file and a
`regions` folder.

If no additional options are specified, then the program will render a map of
the given world, and save a copy into a folder sharing the same name as that of
the world being rendered.

The rendered image's pixels will be colored according to the top-most block at
that coordinate. Glass and "mostly see-through" blocks (torches, rails, fences
etc. but not slabs or stairs) are treated the same as air blocks, and water is
rendered opaque blue (to map the ocean floor, see the [heightmap](#modes) mode
described below).

The full map will have the name `world.png`, and next
to it will be a folder named `regions` containing a separate image for each
region file in the original world's folder.

## Options

`[options]` can be one or more of the following:

| Option      | Description |
|-------------|     ---     |
| `--output=X`| Set the location of the output files. Since omcmap will save more than one file each time it is run, `X` should be the path to a folder, not the name of a file. |
| `--mode=X`  | `X` can be one of `normal`, `heightmap`, `heightmap-color`, or `blocklight`. Details below. |


### Modes

- `normal`: The default behavior of the program, as described above.
- `heightmap`: Renders a greyscale image where each pixel corresponds to the
  y-coordinate of the highest block at that point. It ignores the same blocks as
  the `normal` mode, with the addition of water. The whiter the pixel, the 
  higher the block.
- `heightmap-color`: Similar to `heightmap`, but the output image is based on a
  "heatmap"-like scale, where lower elevations are black/purple/blue, and higher
  elevations are red/brown/white. The colors are taken from the
  `heightmap-colors.png`. Colors may be customized by editing that file.
- `blocklight`: Renders a greyscale image where the brightness of each pixel
  corresponds to the block light level just above the top-most block (where
  'top-most' is determined by the same rules as in the `normal` mode). Black
  pixels correspond to light level zero, and the brighter the pixel, the higher
  the block light at that point. Block light is **not** sunlight - it is light
  that originates from torches, lava, and other light-emitting blocks. Note that
  if there is a solid block at y-level 127, then the light level is 0. There is
  no block above it, so you can't measure the block light at that point, so it
  is rendered on the image as if it's 0.
